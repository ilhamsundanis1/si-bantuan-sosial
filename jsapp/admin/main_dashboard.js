const acces_url = {
	getDtksNonDtksKec : SITE_URL+'Main/GraphDtKsNonDtksKec'
}
const initAjax = (service) => {
    var dataArray = $.ajax({
        type: "GET",
        async: false,
        dataType: "json",
        beforeSend: function () {
            //ProgressBar("wait");
        },
        url: service,
    });

    var result = dataArray.responseJSON; //JSON.stringify

    return result;
};

$(document).ready(function(){
	getChartDtksNonDtksKec();
});



function getChartDtksNonDtksKec(){

	let dataChart1 = initAjax(acces_url.getDtksNonDtksKec);
	let DataProviders = [];

	$.each(dataChart1.data,(i,v) => {
		DataProviders.push({
			category : v.nama_kecamatan,
			column1 : v.dtks,
			column2 : v.no_dtks,
		})
	});

	console.log(DataProviders);
	AmCharts.makeChart("chartdiv",
		{
			"type": "serial",
			"categoryField": "category",
			"autoMarginOffset": 40,
			"marginRight": 70,
			"marginTop": 70,
			"startDuration": 1,
			"fontSize": 13,
			"theme": "patterns",
			"categoryAxis": {
				"gridPosition": "start",
				"labelRotation":25,
			},
			"chartScrollbar": {
				"color": "FFFFFF"
			},
			"responsive": {
			"enabled": true
			},
			"trendLines": [],
			"graphs": [
				{
					"balloonText": "[[title]] of [[category]]:[[value]]",
					"fillAlphas": 0.9,
					"id": "AmGraph-1",
					"title": "DTKS",
					"type": "column",
					"valueField": "column1"
				},
				{
					"balloonText": "[[title]] of [[category]]:[[value]]",
					"fillAlphas": 0.9,
					"id": "AmGraph-2",
					"title": "Non DTKS",
					"type": "column",
					"valueField": "column2"
				}
			],
			"guides": [],
			"valueAxes": [
				{
					"id": "ValueAxis-1",
					"title": "Berdasarkan Kecamatan"
				}
			],
			"allLabels": [],
			"balloon": {},
			"titles": [
				{
                    "id": "Title-1",
                    "size": 15,
                    "text": 'DTKS dan Non DTKS Kecamatan'
                }
			],
			"dataProvider": DataProviders
		}
	);

}
