
const acces_url = {
	GetDataKpm : SITE_URL+"verifikasi/getDataKpm",
	saveValidasi : SITE_URL+"verifikasi/saveVerifikasi",
}

var tableKpm;
var tableVerifikasi;

$(document).ready(function(){

	tableVerifikasi = $("#tableVerifikasiContent").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,25, 50, 100, 200, 'All']],
		"paging": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": SITE_URL + "Verifikasi/get",
			"type": "POST",
			"datatype": "json",
			/* "data": function (d) {      				


				d.kode_kec   = $('#kecamatans').val();//3201161
				d.kode_desa  = $('#desas').val();
				d.bantuan 	 = $('#jenisBantuans').val();
            } */
		},
	
		"columns": [		
			
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},
			{ "data": "nama", "name": "nama", orderable: true,className: 'text-center' },
			{ "data": "nik", "name": "nik", orderable: true ,className: 'text-center'},
			{ "data": "alamat", "name": "alamat", orderable: true,className: 'text-center' },
			{ "data": "nama_bantuan", "name": "nama_bantuan", orderable: false },
			{ "data": "status_verifikasi", "name": "status_verifikasi", orderable: false },
			{ "data": "foto_ktp", "name": "foto_ktp", orderable: false },
			{ "data": "foto_rumah", "name": "foto_rumah", orderable: false },
		]
	   
	});
})

let AddVerifikasi = () => {	
	document.getElementById("form_proses").style.display = "";
	document.getElementById("tableVerifikasi").style.display = "none";
	document.getElementById("addBtnVerifikasi").style.display = "none";

}

let onCancel = () =>{
	document.getElementById("form_proses").style.display = "none";
	document.getElementById("tableVerifikasi").style.display = "";
	document.getElementById("addBtnVerifikasi").style.display = "";
}



$('#addKpmTable').on('click',function(){
	$('#ModalFormKpm').modal("show");



	tableKpm = $("#datatableKpm").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,25, 50, 100, 200, 'All']],
		"paging": true,
		"destroy": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url":acces_url.GetDataKpm,
			"type": "POST",
			"datatype": "json",
			/* "data": function (d) {      				


				d.kode_kec   = $('#kecamatans').val();//3201161
				d.kode_desa  = $('#desas').val();
				d.bantuan 	 = $('#jenisBantuans').val();
            } */
		},	
		"columns": [		
			
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},
			{ "data": "id_kpm", "name": "id_kpm", orderable: true ,className: 'text-center'},
			{
                "render": function (data, type, full, meta) {
					
                    return '<a href="javascript:void(0)" class="" >' + full.nama + '</a>'
                }
            },
			{ "data": "nik", "name": "nik", orderable: true ,className: 'text-center'},
			{ "data": "alamat", "name": "alamat", orderable: true,className: 'text-center' },
		]
	   
	});


	// event onclick modal op
    $('#datatableKpm tbody').on('click', 'a', function () {
        var data = tableKpm.row($(this).parents('tr')).data();
		$("#ModalFormKpm").modal("hide");
		$('#nama_kpm').val(data.nama);
		$('#id_kpm').val(data.id_kpm);
    });

});

onSubmitVerifikasi = () => {
	let keterangan = $('input[name="RadioKeterangan"]:checked').val();
	let id_kpm = $('#id_kpm').val();
	var formData = new FormData($('#formVerifikasi')[0]);
	formData.append('id_kpm',id_kpm);
	formData.append('photo_ktp', $('input[type=file][name="fotoKtp"]')[0].files[0]);
	formData.append('photo_kk', $('input[type=file][name="fotoKk"]')[0].files[0]);
	formData.append('keterangan',keterangan);
	$.ajax({
		url:acces_url.saveValidasi,
		type: 'POST',
		dataType: 'json',
		data: formData,
		contentType: false,
		processData: false,
		beforeSend:function(){
			$("body").css("cursor", "progress");
			$("#loading").removeAttr("style");
		},
		success:function(response) {
		   $("body").css("cursor", "default");
		   if(response.state != true){
				alert("data gagal diinput");
		   }else{
				alert(response.msg);
				document.getElementById("form_proses").style.display = "none";
				document.getElementById("tableVerifikasi").style.display = "";
				document.getElementById("addBtnVerifikasi").style.display = "";
				tableVerifikasi.ajax.reload();
		   }
			
		}
	})

}





