
var tableComplaint;

$(document).ready(function(){
	
	tableComplaint = $("#tablePengaduan").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,25, 50, 100, 200, 'All']],
		"paging": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": SITE_URL + "Complaint/get",
			"type": "POST",
			"datatype": "json",		
		},
	
		"columns": [
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},			
			{ "data": "nama_pelapor", "name": "nama_pelapor", orderable: true ,className: 'text-center'},
			{ "data": "nik_pelapor", "name": "nik_pelapor", orderable: true,className: 'text-center' },
			{ "data": "alamat_pelapor", "name": "alamat_pelapor", orderable: true,className: 'text-center' },			
			{ "data": "desa_pelapor", "name": "desa_pelapor", orderable: false },
			{ "data": "kecamatan_pelapor", "name": "desa_pelapor", orderable: false },
			{ "data": "nik_terlapor", "name": "nik_terlapor", orderable: false },
			{ "data": "nama_terlapor", "name": "nama_terlapor", orderable: false },
			{ "data": "alamat_terlapor", "name": "alamat_terlapor", orderable: false },
			{ "data": "nama_desa", "name": "nama_desa", orderable: false },			
			{ "data": "nama_kec", "name": "nama_kec", orderable: false },
			{ "data": "nama_bantuan", "name": "nama_bantuan", orderable: false },
			{ "data": "keterangan_pelaporan", "name": "keterangan_pelaporan", orderable: false },
		]
	   
	});
})
