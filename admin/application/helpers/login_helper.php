<?php
/*
* @author: safari.erie@gmail.com
* @created: 2020-05-15
*/
if (! defined('BASEPATH'))
	exit ('No direct script access allowed');
    if(!function_exists('get_hash')){
        function get_hash($PlainPassword){
            $option=[
                'sibos'=>5,// proses hash sebanyak: 2^5 = 32x
            ];
            return password_hash($PlainPassword, PASSWORD_DEFAULT, $option);
        }
    }
    if(!function_exists('hash_verified')){
        function hash_verified($PlainPassword,$HashPassword){
			echo $PlainPassword,$HashPassword;
            return password_verify($PlainPassword,$HashPassword) ? true : false;
        }
    }
