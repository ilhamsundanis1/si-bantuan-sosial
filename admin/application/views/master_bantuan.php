	<style>
		.modal-full {
			max-width: 98%;
		}
	</style>
  <div class="main-content">
	
	<section class="section">
		<div class="section-header">
            <h1>Daftar Master Bantuan</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Daftar Master Bantuan</a></div>
                <div class="breadcrumb-item"><a href="#">Admin</a></div>
            </div>
        </div>
		<div class="card" id="ListData" style="">
                <div class="card-header form-row">
                    <h4 class="col-md-6 col-sm-3">Daftar Master Bantuan</h4>
					<div class="col-md-6 text-right"> 
                        <button type="button" class="btn btn-primary btn-flat" id="addBtnMasterBantuan" onclick="AddMasterBantuan();"><i class="fa fa-plus"></i> Master Bantuan</button> &nbsp; &nbsp; &nbsp; 
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive" id="tableMasterBantuan">
							<table id="tableMasterBantuanContent" class="table table-condensed table-bordered table-colored table-custom m-0" style="width:100%">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Bantuan</th>
										<th>Keterangan</th>					
										<th>Status</th>					
										<th>Aksi</th>					
									</tr>
								</thead>
							</table>
                    </div>
					<div class="row" id="form_proses" style="display:none;">
					
						<div class="col-sm-12">
							<form class="form-horizontal" id="formMasterBantuan"  enctype="multipart/form-data" role="form" method="post" action="">
								<div class="row">
									<div class="col-12">
										<div class="form-group row">
											<label class="col-md-1 control-label text-left">
												<div class="">
													Nama KPM
												</div>
											</label>
											<div class="col-md-5">
												<input type="text" class="form-control" disabled="disabled" id="nama_kpm" name="name_kpm">
												<input type="hidden" class="form-control" id="id_kpm" name="id_kpm">
												
											</div>
											<div class="col-md-4">
												<button type="button" class="btn btn-warning" name="addKpmTable" id="addKpmTable">Pilih Kpm</button>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-1 control-label text-left">
												<div class="">
													Keterangan
												</div>
											</label>
											<div class="col-md-5">
												<div class="form-check">
													<input class="form-check-input" type="radio" name="RadioKeterangan" value="layak" id="RadioKeterangan1" checked="">
													<label class="form-check-label" for="RadioKeterangan1">
														Layak
													</label>
												</div>
												<div class="form-check">
													<input class="form-check-input" type="radio" name="RadioKeterangan" value="meninggal" id="RadioKeterangan2">
													<label class="form-check-label" for="RadioKeterangan2">
														Meninggal
													</label>
												</div>
												<div class="form-check">
													<input class="form-check-input" type="radio" name="RadioKeterangan" value="pindah" id="RadioKeterangan3" >
													<label class="form-check-label" for="RadioKeterangan3">
														Pindah
													</label>
												</div>
												<div class="form-check">
													<input class="form-check-input" type="radio" name="RadioKeterangan" value="Tidak Mampu" id="RadioKeterangan4" >
													<label class="form-check-label" for="RadioKeterangan4">
														Tidak Mampu
													</label>
												</div>
												<div class="form-check">
													<input class="form-check-input" type="radio" name="RadioKeterangan" value="menerima bantuan lain" id="RadioKeterangan5" >
													<label class="form-check-label" for="RadioKeterangan5">
														Sudah Menerima Bantuan Lain
													</label>
												</div>
											</div>
											
										</div>
										<div class="form-group row">
											<label class="col-md-1 control-label text-left">
												<div class="">
													Foto Ktp
												</div>
											</label>
											<div class="col-md-5">												
												<input type="file" name="fotoKtp" id="fotoKtp" class="form-control">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-1 control-label text-left">
												<div class="">
													Foto Rumah
												</div>
											</label>
											<div class="col-md-5">												
												<input type="file" name="fotoKk" id="fotoKk" class="form-control">
											</div>
										</div>
									</div>
								</div>

								<hr />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group text-right">
                                        <button type="button" class="btn btn-secondary btn-bordered waves-effect w-md m-b-5" onclick="onCancel()">Batal</button>
                                        <button type="button" class="btn btn-success btn-bordered waves-effect w-md waves-light m-b-5" onclick="onSubmitMasterBantuan()">Simpan</button>
                                    </div>
                                </div>
                            </div>
							</form>
						</div>
					</div>
                </div>
            </div>
		



			<div class="card" id="formInput" style="display:none">
				<div class="card-header form-row">
					<h4 class="col-md-6 col-sm-3" id="info-title-input">Input Bantuan</h4>
				</div>
				<div class="card-body">
					<div class="row" id="form_proses_bantuan" >
						<div class="col-md-12">
							<form class="form-horizontal" id="formBantuan"  enctype="multipart/form-data" role="form" method="post" action="">
								<div class="row">
									<input type="hidden" name="id_bantuan" id="id_bantuan" value="-1"/>
									<div class="col-12">
										<div class="form-group row">
											
											<label class="col-md-3 control-label text-left pt-2">
												<div class="">
													Nama Bantuan
												</div>
											</label>
											<div class="col-md-6">
												<input type="text" class="form-control"  id="nama_bantuan" name="nama_bantuan">
											</div>
										</div>										

										<div class="form-group row">
											<label class="col-md-3 control-label text-left pt-2">
												<div class="">
													Keterangan 
												</div>
											</label>
											<div class="col-md-8">
												<textarea class="form-control" id="keterangan" name="keterangan" style="height:90px"></textarea>
											</div>
										</div>									
										
										<div class="form-group row">
											<label class="col-md-3 control-label text-left pt-2">
												<div class="">
													Status 
												</div>
											</label>
											<div class="col-md-4">
												<select name="cmb_status" id="cmb_status" class="form-control">
													<option value="1">Aktif</option>
													<option value="0">Tidak Aktif</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<hr>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group text-right">
											<button type="button" class="btn btn-secondary btn-bordered waves-effect w-md m-b-5" onclick="onCancel()">Batal</button>
											<button type="button" class="btn btn-success btn-bordered waves-effect w-md waves-light m-b-5" onclick="onSubmitBantuan()">Simpan</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	
	
	
	
	</section>


	<div id="modal-cant-delete" class="modal fade" 
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">            

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
			<div class="modal-body">
					<div id="text_confirm">
					</div>
                </div>
                <div class="modal-footer">             
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div id="modal-delete" class="modal fade" 
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">            

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <form id="form_delete_kpm" class="form-horizontal" enctype="multipart/form-data" role="form" method="post" action="">
                
                <div class="modal-body">
                    <p class="text-center" id="text_confirm_delete"></p>
                    <strong><p class="text-center" id="text_delete"></p></strong>
                    <p class="text-center" id="text_confirm_info"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect waves-light" onclick="" data-dismiss="modal" id="btn_confirm">Yes</button>
                    
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>


<!-- modal confem delete -->


</div>
