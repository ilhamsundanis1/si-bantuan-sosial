
  <div class="main-content">
	
	<section class="section">
		<div class="section-header">
            <h1>Daftar Pengaduan Bansos</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Management Bansos</a></div>
                <div class="breadcrumb-item"><a href="#">Admin</a></div>
                <div class="breadcrumb-item active"><a href="#">Daftar Pengaduan</a></div>
            </div>
        </div>
		<div class="card" id="ListData" style="">
                <div class="card-header form-row">
                    <h4 class="col-md-6 col-sm-3">Daftar Pengaduan Bansos</h4>
                   
                </div>
                <div class="card-body">
                    <div class="table-responsive">
							<table id="tablePengaduan" class="table table-condensed table-bordered table-colored table-custom m-0" 
							style="width:100%">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Pelapor</th>
										<th>Nik Pelapor</th>
										<th>Alamat Pelapor</th>
										<th>Desa Pelapor</th>
										<th>Kecamatan Pelapor</th>
										<th>Nik Terlapor</th>
										<th>Nama Terlapor</th>
										<th>Alamat Terlapor</th>
										<th>Desa Pelapor</th>						
										<th>Kecamatan Pelapor</th>	 				
										<th>Nama Bantuan</th>	 				
										<th>Keterangan Pelaporan</th>	 				
									</tr>
								</thead>
							</table>
                    </div>
                </div>
            </div>
		
		
	</section>
</div>
