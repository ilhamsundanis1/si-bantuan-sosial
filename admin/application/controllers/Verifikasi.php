<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikasi extends CI_Controller {

	function __construct() {
        parent:: __construct();
        $this->load->model('verifikasi_model','vm');
	}

	function index(){
		
		$data['jsapp'] = array('admin/verifikasi');
		$this->load->view('header',$data);
		$this->load->view('verifikasi_bansos');
		$this->load->view('footer');

	}

	function add(){
		$data['jsapp'] = array('admin/addverifikasi');
		$this->load->view('header',$data);
		$this->load->view('add_verifikasi');
		$this->load->view('footer');

	}

	function getKpmByWilayah(){
		
		
		$data = $this->vm->getKpmByKodeWilayah();
		$result['Data'] = $data;
		echo json_encode($result);
	}

	function getDataKpm(){
		
		$order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		$nama = $this->input->post('nama');

		$result = array();
        $recordsTotal = $this->vm->count_data_kpm($nama);
		
        $row = array();
		$results = $this->vm->getKpmByKodeWilayah($length,$start, $def['order'], 'asc',$nama);
        $dd = "";

        if (count($results) > 0) {
			$ii = $start;
			   
            foreach ($results as $d) {
				$ii++;
				
                $row[] = array
				(
					"no"				=> $ii,
					"id_kpm"				=> $d->id_kpm,
					"nama" 			=> $d->nama,
					"nik" 			=> $d->nik,
					"alamat" 			=> $d->alamat,
				);
            }
        }
      
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);

	}

	function saveVerifikasi(){

		$id_kpm = $this->input->post('id_kpm');
		$photo_ktp = $this->input->post('photo_ktp');
		$photo_kk = $this->input->post('photo_kk');
		$keterangan = $this->input->post('keterangan');
		
		$lastidverifikasi = $this->vm->getLastVerifikasi()->lastidverifikasi;

		$photoKtp = "";
		if (isset($_FILES['photo_ktp']['name']) && !empty($_FILES['photo_ktp']['name'])) {
			$photoKtp = $this->do_upload_ktp($id_kpm);
		}else{
			$photoKtp = null;
		}

		$photoRumah = "";
		if (isset($_FILES['photo_kk']['name']) && !empty($_FILES['photo_kk']['name'])) {
			$photoRumah = $this->do_upload_kk($id_kpm);
		}else{
			$photoRumah = null;
		}
		
		
		$dataInsertVerifikasi = array(
			'id_verifikasi' => $lastidverifikasi,
			'id_kpm'	=> $id_kpm,
			'hasil'		=> $keterangan,
			'foto_ktp'	=> $photoKtp,
			'foto_rumah'	=> $photoRumah,
			'created_by' => $this->session->userdata(S_ID_USER),
			'created_dt' => date('Y-m-d H:i:s')
		); 
		$insert_data = $this->vm->saveVerifikasi($dataInsertVerifikasi);
		$output = array(
			'state'	=> $insert_data,
			'msg'	=> 'Data Berhasil diinput'
		);
		echo json_encode($output);
	}


	function do_upload_ktp($photo) 
	{	
		
		
		$file_path 	  = './uploads/kk_ktp';
		
		$temp_file_lama = $photo;
		if ($temp_file_lama != '') 
		{
			$filep = $file_path . $temp_file_lama;
			if (file_exists($filep)) {
				unlink($filep);
			}
		}
			
		$config['file_name']        = "KTP_".$photo.'_'.time();
		$config['upload_path'] 		= $file_path;
		$config['allowed_types'] 	= 'jpg|jpeg|gif|png';
		$config['max_size'] 		= '20000';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload('photo_ktp')){
			$error = array('error' => $this->upload->display_errors());
			echo json_encode($error);
		}else{
			$file_data 		= $this->upload->data();
			$nama_filenya 	= $file_data['file_name'];
		
			return $nama_filenya;
		}

		
		
	}
	
	function do_upload_kk($photo) 
	{	
		$file_path 	  = './uploads/kk_ktp';
		$config['file_name']        = "FOTO_RUMAH_".$photo.'_'.time();	
		$config['upload_path'] 		= $file_path;
		$config['allowed_types'] 	= 'jpg|jpeg|gif|png';
		$config['max_size'] 		= '20000';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( !$this->upload->do_upload('photo_kk')){
			$error = array('error' => $this->upload->display_errors());
			echo json_encode($error);
		}else{
			$file_data 		= $this->upload->data();
			$nama_filenya 	= $file_data['file_name'];
		
			return $nama_filenya;
		}

		
		
	}
	

	function get(){
		$order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		
		

		$result = array();
        $recordsTotal = $this->vm->countVerifikasi();
		
        $row = array();
		$results = $this->vm->getDataVerifikasi($length,$start, $def['order'], 'asc');
        $dd = "";

        if (count($results) > 0) {
			$ii = $start;
			   
            foreach ($results as $d) {
				$ii++;
				
                $row[] = array
				(
					"no"					=> $ii,
					'nama'					=> $d->nama,
					"nik"					=> $d->nik,
					"alamat"					=> $d->alamat,
					"nama_bantuan"					=> $d->nama_bantuan,
					"status_verifikasi" 	=> ($d->status_verifikasi == 1) ? 'Sudah Verifikasi' : 'Belum Verifikasi',
					"keterangan" 			=> $d->keterangan,
					"foto_ktp" 				=> '<img class="img-thumbnail" src="'. base_url().'admin/uploads/kk_ktp/'.$d->foto_ktp.'">',
					"foto_rumah" 			=> '<img class="img-thumbnail" src="'. base_url().'admin/uploads/kk_ktp/'.$d->foto_rumah.'">',
				);
            }
        }
      
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);
	}

}
