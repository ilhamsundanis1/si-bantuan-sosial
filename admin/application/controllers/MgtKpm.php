<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MgtKpm extends CI_Controller {

	function __construct() {
        parent:: __construct();
        $this->load->model('Pkm_model','pm');
        $this->load->model('Wilayah_model','wm');
	}

	public function index()
	{
		$data['jsapp'] = array('admin/mgt_kpm');
		$this->load->view('header',$data);
		$this->load->view('mgtkpm');
		$this->load->view('footer');
	}


	function get() {
        $order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		
		$kode_kec = $this->input->post('kode_kec');
		
		$kode_desa = $this->input->post('kode_desa');
		$nik = $this->input->post('nik');

		$result = array();
        $recordsTotal = $this->pm->count_data_pkm($kode_kec, $kode_desa, $nik);
		/* print_r($this->db->last_query());
		die; */
        $row = array();
		$results = $this->pm->get_data($length,$start, $def['order'], 'asc', $kode_kec, $kode_desa, $nik);
        $dd = "";

        if (count($results) > 0) {
			$ii = $start;
            foreach ($results as $d) {
				$ii++;
                $row[] = array
				(
					"no"				=> $ii,
					"nama" 				=> substr_replace($d->nama, '..XXX', -3, 1) ,
					"nik" 				=> substr_replace($d->nik, '..XXX', -3, -1) ,
					"alamat" 			=> $d->alamat,
					"kecamatan" 		=> $d->nama_kec,
					"desa" 				=> $d->nama_desa,
					"status" 			=> ($d->status == true) ? "AKTIF" : "TIDAK",
					"status_dtks" 	    => ($d->dtks == true) ? "YA" : "TIDAK",
					"aksi" 	    		=> '<button type="button" class="btn btn-info btn-flat btn-sm" onclick="EditKpm('.$d->id_kpm.');"><i class="fa fa-edit"></i> Edit KPM</button> <button type="button" class="btn btn-danger btn-flat btn-sm" onclick="HapusKpm('.$d->id_kpm.');"><i class="fa fa-trash"></i> Hapus KPM</button>'
				);
            }
        }
      
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);
	}
	

	function getKecamatan()
	{

		$data = $this->wm->getKec();
		$result['Data'] = $data;
		echo json_encode($result);
	}

	function getDesa(){
		$kode_kec = $this->input->post('kode_kec');
		
		$data = $this->wm->getDesa($kode_kec);
		$result['Data'] = $data;
		echo json_encode($result);

	}

	function getBantuan(){
	
		
		$data = $this->wm->getBantuan();
		$result['Data'] = $data;
		echo json_encode($result);
	}

	function SaveKpm(){
		$id_kpm = $this->input->post('id_kpm');
		$kode_desa = $this->input->post('kode_desa');
		$nama = $this->input->post('nama');
		$nik = $this->input->post('nik');
		
		$alamat = $this->input->post('alamat');
		$layak = $this->input->post('layak');
		$status = $this->input->post('status');
		$dtks = $this->input->post('dtks');
		$getLastId = $this->pm->getLastIdKpm()->lastid;

		if($id_kpm == -1){
			$data_insert = array(
				'id_kpm'	=> $getLastId,
				'kode_desa'	=> $kode_desa,
				'nama'	=> $nama,
				'nik'	=> $nik,
				'alamat'	=> $alamat,
				'layak'	=> $layak,
				'status'	=> $status,
				'dtks'	=> $dtks,
				'created_by' => $this->session->userdata(S_ID_USER),
				'created_dt' => date('Y-m-d H:i:s')
			);
	
			$insert_kpm = $this->pm->SaveKpm($id_kpm,$data_insert);
	
			if($insert_kpm){
				$output = array(
					'state'	=> true,
					'msg'	=> 'Data Berhasil diinput'
				);
	
				echo json_encode($output);
			}else{
				$output = array(
					'state'	=> false,
					'msg'	=> 'Data gagal diinput'
				);
				echo json_encode($output);
			}
		}else{
			$data_update = array(
				'kode_desa'	=> $kode_desa,
				'nama'	=> $nama,
				'nik'	=> $nik,
				'alamat'	=> $alamat,
				'layak'	=> $layak,
				'status'	=> $status,
				'dtks'	=> $dtks,
				'updated_by' => $this->session->userdata(S_ID_USER),
				'updated_dt' => date('Y-m-d H:i:s')
			);
	
			$insert_kpm = $this->pm->SaveKpm($id_kpm,$data_update);
	
			if($insert_kpm){
				$output = array(
					'state'	=> true,
					'msg'	=> 'Data Berhasil dirubah'
				);
	
				echo json_encode($output);
			}else{
				$output = array(
					'state'	=> false,
					'msg'	=> 'Data gagal dirubah'
				);
				echo json_encode($output);
			}
		}
	}


		

	function GetKpmById(){

		$idKPm = $this->input->post('IdKpm');
		
		$dataKpm  = $this->pm->getKpmById($idKPm);

		if(count($dataKpm) > 0){
			$dataTemp = array(
				'id_kpm'	=> $dataKpm->id_kpm,
				'kode_desa' => $dataKpm->kode_desa,
				'kode_kec' => $dataKpm->kode_kec,
				'nama' => $dataKpm->nama,
				'nik' => $dataKpm->nik,
				'alamat' => $dataKpm->alamat,
				'layak' => $dataKpm->layak,
				'status' => $dataKpm->status,
				'dtks' => $dataKpm->dtks,
			);
			$output = array(
				'state'	=> true,
				'msg'	=> 'Data ada',
				'data'	=> $dataTemp
			);
			echo json_encode($output);
		}else{
			$output = array(
				'state'	=> false,
				'msg'	=> 'Data KPM berdasarkan Id '.$idKpm.' tidak ada',
				'data'	=> null
			);
			echo json_encode($output);
		}

	}

	function GetKpmDataDelete(){
		$idKpm = $this->input->post('IdKpm');
		
		$checkBansos = $this->pm->checkBansos($idKpm);
		if(count($checkBansos) != 0){
			$output = array(
				'state'	=> false,
				'msg'	=> ' Tidak bisa di hapus, sudah ada di Penerima Bansos'
			);
			echo json_encode($output);
		}else{
			$output = array(
				'state'	=> true,
				'msg'	=> ' '
			);
			echo json_encode($output);
		}
	}

	function KpmDelete(){

		$idKpm = $this->input->post('IdKpm');
		$arr_where = array(
			'id_kpm'	=> $idKpm
		);
		$deleteKpm = $this->pm->KpmDelete($arr_where,'bansos.tm_kpm');
		if($deleteKpm){ 
			$output = array(
				'state'	=> true,
				'msg'	=> ' Data Kpm Berhasil dihapus'
			);
			echo json_encode($output);
		}else{
			$output = array(
				'state'	=> false,
				'msg'	=> ' Data Gagal Dihapus'
			);
			echo json_encode($output);
		}

	}


}
