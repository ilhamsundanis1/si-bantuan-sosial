
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SIBOS - Sistem Infomrasi Bantuan Sosial</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
	<link type="text/css" href="<?php echo base_url();?>assets/frontEnd/img/logo_sibos.png" rel="icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url(); ?>assets/frontEnd/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/frontEnd/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/frontEnd/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/frontEnd/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/frontEnd/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/frontEnd/vendor/aos/aos.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/plugin/datatable/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

	<!-- datatables -->



  <!-- Template Main CSS File -->
  <link href="<?php echo base_url(); ?>assets/frontEnd/css/style.css" rel="stylesheet">

  <script type="text/javascript">
    var SITE_URL = '<?php echo site_url() ?>';
    //uri string ada di my_controller
    var CONTROLLER = '<?php echo ($this->uri->segment(1) !== FALSE) ? $this->uri->segment(1) : ""; ?>';

    // console.log('tes >> '+CONTROLLER);
	</script>
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container-fluid d-flex">

      <div class="logo mr-auto">
        <h1 class="text-light">
					<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/frontEnd/img/logo_sibos.png"></a></h1>
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="#hero"><i class="fa fa-home" aria-hidden="true"></i>Beranda</a></li>
          <li><a href="#about"><i class="fa fa-bullhorn" aria-hidden="true"></i>Pengaduan</a></li>
          <li> <a href="#contact">Pencarian</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->
