<!-- ======= Footer ======= -->
<footer id="footer">
<!-- 
<div class="footer-top">
  <div class="container">
	<div class="row">

	  <div class="col-lg-3 col-md-6 footer-contact" data-aos="fade-up" data-aos-delay="100">
		<h3>Ninestars</h3>
		<p>
		  A108 Adam Street <br>
		  New York, NY 535022<br>
		  United States <br><br>
		  <strong>Phone:</strong> +1 5589 55488 55<br>
		  <strong>Email:</strong> info@example.com<br>
		</p>
	  </div>

	  <div class="col-lg-3 col-md-6 footer-links" data-aos="fade-up" data-aos-delay="200">
		<h4>Useful Links</h4>
		<ul>
		  <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
		  <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
		  <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
		  <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
		  <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
		</ul>
	  </div>

	  <div class="col-lg-3 col-md-6 footer-links" data-aos="fade-up" data-aos-delay="300">
		<h4>Our Services</h4>
		<ul>
		  <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
		  <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
		  <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
		  <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
		  <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
		</ul>
	  </div>

	  <div class="col-lg-3 col-md-6 footer-links" data-aos="fade-up" data-aos-delay="400">
		<h4>Our Social Networks</h4>
		<p>Cras fermentum odio eu feugiat lide par naso tierra videa magna derita valies</p>
		<div class="social-links mt-3">
		  <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
		  <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
		  <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
		  <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
		  <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
		</div>
	  </div>

	</div>
  </div>
</div> -->

<div class="container py-4">
  <div class="copyright">
	&copy; Copyright <strong><span>Dinas Sosial Kabupaten Bogor</span></strong>. All Rights Reserved
  </div>
  <div class="credits">
	<!-- Designed by <a href="https://bootstrapmade.com/">Ilham Sundanis</a> -->
  </div>
</div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>



<!-- Vendor JS Files -->
<script src="<?php echo base_url();?>assets/frontEnd/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/frontEnd/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url();?>assets/frontEnd/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="<?php echo base_url();?>assets/frontEnd/vendor/php-email-form/validate.js"></script>
<script src="<?php echo base_url();?>assets/frontEnd/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url();?>assets/frontEnd/vendor/venobox/venobox.min.js"></script>
<script src="<?php echo base_url();?>assets/frontEnd/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/frontEnd/vendor/aos/aos.js"></script>  
<script src="<?php echo base_url();?>assets/plugin/datatable/1.10.19/js/jquery.dataTables.js"></script>  
<script src="<?php echo base_url();?>assets/plugin/datatable/1.10.19/js/dataTables.bootstrap4.min.js"></script>  
<script src="https://use.fontawesome.com/846b20144e.js"></script>


<!-- Template Main JS File -->
<script src="<?php echo base_url();?>assets/frontEnd/js/main.js"></script>

<!-- jsapp -->
  <?php
if (isset($jsapp)) : foreach ($jsapp as $js) : ?>
    <script type="text/javascript" src="<?php echo base_url() ?>jsapp/<?php echo $js ?>.js"></script>
<?php
  endforeach;
endif;
?>


</body>

</html>
