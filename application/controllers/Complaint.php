
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Complaint extends CI_Controller {

	function __construct() {
        parent:: __construct();
        $this->load->model('Complaint_model','cm');
        $this->load->model('Wilayah_model','wm');
	}

	function index(){
		$data['jsapp'] = array('FrontEnd/complaint');
		$id_kpm = $this->input->get('ids');
		$data['dt_kpm'] = $this->cm->get_id_kpm($id_kpm);
		$data['dt_kec'] = $this->wm->getKec();
		$this->load->view('header',$data);
		$this->load->view('complaint');
		$this->load->view('footer');
	}

	function savePengaduan(){

		$id_desa 	 		= $this->input->post('id_desa');
		$nik_pengadu 		= $this->input->post('nik_pengadu');
		$nama_pengadu		= $this->input->post('nama_pengadu');
		$alamat				= $this->input->post('alamat');
		$id_jenis_pengaduan	= $this->input->post('id_jenis_pengaduan');		
		$id_kpm = $this->input->post('id_kpm');
		$no_tlp 			= $this->input->post('no_tlp');
		$getLastPengaduan = $this->cm->getLastIdPengaduan()->lastid;
		
		$data_pengaduan = array(
			'id_pengaduan'			=> $getLastPengaduan,
			'id_desa'				=> $id_desa,
			'nik_pengadu'			=> $nik_pengadu,
			'nama_pengadu'			=> $nama_pengadu,
			'alamat'				=> $alamat,
			'id_jenis_pengaduan'	=> $id_jenis_pengaduan,
			'id_kpm'				=> $id_kpm,
			'created_dt'		=> date('Y-m-d H:i:s'),
			'no_telp'			=> $no_tlp
		);

		$insertPengaduan = $this->cm->savePengaduan($data_pengaduan);
		if($insertPengaduan){
			$output = array(
				'state'	=> true,
				'msg'	=> 'Data Berhasil diinput'
			);

			echo json_encode($output);
		}else{
			$output = array(
				'state'	=> false,
				'msg'	=> 'Data gagal diinput'
			);
			echo json_encode($output);
		}
	}

}
